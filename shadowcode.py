from app import create_app
from config import DevConfig


if __name__ == '__main__':
    app = create_app(config_class=DevConfig)
    app.run()
else:
    app = create_app()




