from flask import Blueprint

bp = Blueprint('api_user', __name__)

from app.services.site_pages import routes