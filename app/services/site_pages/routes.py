from flask import request, render_template
from app.services.site_pages import bp


@bp.route('/', methods=['GET'])
def home():

    return render_template('home.html')
