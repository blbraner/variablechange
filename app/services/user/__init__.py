from flask import Blueprint

bp = Blueprint('site_pages', __name__)

from app.services.user import api_routes
