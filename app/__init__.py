from flask import Flask, request, render_template

from app.models.user import User
from config import Config, TestConfig
from flask_mongoengine import MongoEngine
from flask_user import login_required, UserManager, UserMixin

db = MongoEngine()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)

    user_manager = UserManager(app, db, User)

    # register site pages service
    from app.services.site_pages import bp as site_pages_bp
    app.register_blueprint(site_pages_bp)

    # register user service
    from app.services.user import bp as user_bp
    app.register_blueprint(user_bp)

    # register feature flag service
    from app.services.feature_flag import bp as feature_flag_bp
    app.register_blueprint(feature_flag_bp)
    return app
